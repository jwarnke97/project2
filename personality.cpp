// Shahzoda Staroverov, Jordan Warnke
// 10/17/19
// TCES 203
// Project #2
//
// This program will program that processes an input file of data for a
// personality test known as the Keirsey Temperament Sorter.
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
using namespace std;
//string with possible dimension letters
#define RESULT "ESTJINFP"
//constant for dimensions
#define LEN 8

//Stores all the variables for each participant
struct Personality
{
	string userName;
	string userOutput;
	int counts[LEN*2];
	int percentages[LEN];
	char result[LEN/2-1];

};

int lineCount(string);
Personality* read_file(string, int&);
void personalityResult(Personality*, int);
void scoreCount(Personality*, int);
void percentScore(Personality*, int);
void outputFile(string, const Personality*, int);

//Driver function
int main(){
	Personality* ptr;

	string inFile = "personality.txt";
	string outFile = "personality.out";
	int size = lineCount(inFile)/2;
	ptr = read_file(inFile, size);
	scoreCount(ptr, size);
	percentScore(ptr, size);
	personalityResult(ptr, size);
	outputFile(outFile, ptr, size);
	return 0;

}
//returns amount of lines in input file
int lineCount(string file) {
	string line;
	ifstream inFile(file.c_str());
	int count = 0;
	while (getline(inFile, line))
		count++;
	return count;

}
//reads from input file
Personality* read_file(string file, int& size){
	Personality* ptr;
	ifstream inFile(file.c_str());
	if (!inFile){
		cout <<"File not found"<< endl;
	}
	else{
		ptr = new Personality[size];
		for (int i = 0; i<size; i++){
			getline(inFile, ptr[i].userName);
			getline(inFile, ptr[i].userOutput);
		}

	}
	inFile.close();
	return ptr;
}
//calculates the percentage for B answers
void percentScore(Personality* ptr, int size){
	for (int i = 0; i < size; i++){
		for (int j = 0; j < 4; j++){
			float percent = (1 / ((float)ptr[i].counts[j + 4] +
                  (float)ptr[i].counts[j])) * ptr[i].counts[j + 4] * 100;
			ptr[i].percentages[j] = int(percent + 0.5);
		}
	}
}
//stores personality result in pointer
void personalityResult(Personality* ptr, int size){
	for (int i = 0; i < size; i++){
		for (int j = 0; j<4; j++){
			if (ptr[i].percentages[j] > 50){
				ptr[i].result[j] = RESULT[j + 4];
			}
			else if (ptr[i].percentages[j] < 50){
				ptr[i].result[j] = RESULT[j];
			}
			else{
				ptr[i].result[j] = 'X';
			}
		}

	}
}
//tallys up score for each participants answers
void scoreCount(Personality* ptr, int size)
{
	int scoreAnswers[LEN];

	for (int j=0; j <size; j++){
		for (int i = 0; i < LEN; i++) {
			scoreAnswers[i] = 0;
		}
		cout << ptr[j].userOutput[69]<<endl;
		for (int q = 0; q < 70; q += 7){
			if (ptr[j].userOutput[q] == 'a' || ptr[j].userOutput[q] == 'A'){
				scoreAnswers[0] = scoreAnswers[0] + 1;
			}
			else if (ptr[j].userOutput[q] == 'b' || ptr[j].userOutput[q] == 'B'){
				scoreAnswers[4] = scoreAnswers[4] + 1;
			}
		}
		for (int dim = 1; dim < 4; dim++){
			for (int in = 2*dim-1; in < 70; in+= 6){
				if (ptr[j].userOutput[in] == 'A' || ptr[j].userOutput[in] == 'a'){
					scoreAnswers[dim] = scoreAnswers[dim] + 1;
				}
				else if (ptr[j].userOutput[in] == 'B' || ptr[j].userOutput[in] == 'b'){
					scoreAnswers[dim + 4] = scoreAnswers[dim + 4] + 1;
				}
				in++;

				if (ptr[j].userOutput[in] == 'A' || ptr[j].userOutput[in] == 'a'){
					scoreAnswers[dim] = scoreAnswers[dim] + 1;
				}
				else if (ptr[j].userOutput[in] == 'B' || ptr[j].userOutput[in] == 'b'){
					scoreAnswers[dim + 4] = scoreAnswers[dim + 4] + 1;
				}
			}
		}
		for (int index = 0; index< LEN; index++){
			ptr[j].counts[index] = scoreAnswers[index];
		}
	}

}
//writes to the output file
void outputFile(string file, const Personality* ptr, int size)
{
	ofstream outFile;
	outFile.open(file.c_str());

	outFile << "Output file personality.out (Checkpoint output)" << endl;
	outFile << endl;
	for (int i = 0; i < size; i++){
		outFile << ptr[i].userName << ": ";

		for (int j = 0; j < LEN; j++){
			outFile << ptr[i].counts[j]<< " ";
		}
		outFile << endl;
	}
	outFile << endl;
	outFile << "Output file personality.out (Final output)" << endl;
	outFile <<endl;
	for (int z = 0; z < size; z++){
		outFile << ptr[z].userName << ": [";
		outFile << ptr[z].percentages[0];
		for (int index = 1; index < 4; index++) {
			outFile << ", " << ptr[z].percentages[index];
		}
		outFile << "] = ";
		for (int in = 0; in < 4; in++) {
			outFile << ptr[z].result[in];
		}
		outFile<<endl;
	}

	outFile.close();

}
